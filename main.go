// Utility to extract specific sequences from a fasta file

package main

import (
    "bufio"
    "flag"
    "fmt"
    "log"
    "os"
    "regexp"
    "strings"
    "sync"

    "bitbucket.org/GottySG36/sequtils"
)

var trailing = regexp.MustCompile(`(^\s+)|(\s+$)`)

func GetHeaders(f string) ([]string, error) {
    fi, err := os.Open(f)
    defer fi.Close()
    if err != nil {
        return nil, err
    }

    heads := make([]string, 0)
    r := bufio.NewScanner(fi)
    for r.Scan() {
        if r.Text() == "" {
            continue
        }
        heads = append(heads, trailing.ReplaceAllLiteralString(r.Text(), ""))
    }
    if len(heads) == 0 {
        return nil, fmt.Errorf("EmptyFileError")
    }
    return heads, nil
}

func GetHeadersString(s string) ([]string, error) {
    if s == "" {
        return nil, fmt.Errorf("EmptyHeadersError")
    }
    i := strings.Split(s , ",")
    for idx, it := range i {
        i[idx] = trailing.ReplaceAllLiteralString(it, "")
    }
    return i, nil
}

func Unique(ids *[]string) {
    found := make(map[string]bool)
    n := 0
    for _, seq := range *ids {
        if !found[seq] {
            found[seq] = true
            (*ids)[n] = seq
            n++
        }
    }
    *ids = (*ids)[:n]
}

type desc struct {
    Input   string
    Prefix  string
}

type Description struct {
    Descr   []desc
    Stdout  bool   // Single output 
}

func GetDesc(i, p string) (Description, error) {
    var d Description
    inps  := strings.Split(i, ",")
    if p == "" { p = defaultPrefix }

    prefs := strings.Split(p, ",")

    if prefs[0] == "-" {
        d.Stdout = true
    }
    if len(prefs) < len(inps) {
        prefs_cp := make([]string, len(inps))
        copy(prefs_cp, prefs)
        prefs = prefs_cp
        for idx, v := range prefs {
            if v == "" {
                prefs[idx] = fmt.Sprintf("%v-%v", prefs[0], idx)
            }
        }
    }

    d.Descr = make([]desc, len(inps))

    for idx, _ := range inps {
        d.Descr[idx] = desc{Input:inps[idx], Prefix:prefs[idx]}
    }
    return d, nil
}

var (
    defaultPrefix = "sample"
    defaultOutDir = "."

    synopsis = `Simple utility to read fastq files and extract sequences whose headers have been specified.
Multiple file can be read from in parallel with at most 't' jobs.`
    uString  = `extract_seqs [-fsq <FILE1,...>] [-ids <STR1,...>] [-f-ids <FILE>] [-p <STR1,...] [-t INT] [-exact] [-warn] [-z]`

    fqs = flag.String( "fqs",  "-",       "Input fastqs containing the sequences to look for, comma seperated list")
    ids  = flag.String( "ids",   "",        "List of fasta headers to look for, comma seperated")
    fIds = flag.String( "f-ids", "",        "File containing a list of ids to look for.")
    pref = flag.String( "p",     "-",  "Prefix used to build output file names, has to be a comma seperated list if multiple input files")
    substr = flag.String("substr", "", "Substring to use to limit the matches in case of substring matching in headers")
    jobs = flag.Int(    "t",     1,         "Number of concurrent jobs to run for reading input and writing output")
    exact = flag.Bool(  "exact", false,     "Dictates whether the provided headers are exact or a substring")
    warn = flag.Bool(  "warn", false,     "Will warn if a sequence is not found in fastq file")
    exclude = flag.Bool("exclude", false,   "Reverses selection. Dictates that the provided headers are to be removed instead.")
    zip     = flag.Bool("z", false, "Specifies that the output result is to be gzip-compressed.")
)


var Usage = func() {
    fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", synopsis)
    fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uString)

    fmt.Fprintf(flag.CommandLine.Output(), "Arguments of %s:\n", os.Args[0])
    flag.PrintDefaults()
}

func main() {
    flag.Usage = Usage
    flag.Parse()
    d, err := GetDesc(*fqs, *pref)
    if err != nil {
        log.Fatalf("Error -:- GetDesc : %v\n", err)
    }

    var headersStr []string
    var headersFi  []string
    var headers    []string
    if *ids != "" {
        headersStr, err = GetHeadersString(*ids)
        if err != nil {
            log.Fatalf("Error -:- GetHeadersString : %v\n", err)
        }
    }
    if *fIds != "" {
        headersFi, err = GetHeaders(*fIds)
        if err != nil {
            log.Fatalf("Error -:- GetHeaders : %v\n", err)
        }
    }
    headers = append(headersFi, headersStr...)
    Unique(&headers)

    threads := make(chan struct{}, *jobs)
    res := make(map[string]sequtils.Fsq, len(d.Descr))

    var wg sync.WaitGroup
    var lock sync.Mutex

    for _, inp := range d.Descr {
        wg.Add(1)
        threads <- struct{}{}
        go func(d desc, ids []string, sub string, x, excl, w, s bool) {
            defer func() {
                wg.Done()
                <-threads
            }()
            fq, err := sequtils.LoadFastq(d.Input)
            if err != nil {
                log.Fatalf("Error -:- LoadFastq : %v\n", err)
            }

            fIdx, err := fq.Index()
            if err != nil {
                log.Fatalf("Error -:- Index : %v\n", err)
            }

            err = fq.FilterSequences(ids, fIdx, sub, x, excl, w)
            if err != nil {
                log.Fatalf("Error -:- FilterSequences : %v\n", err)
            }

            if s {
                lock.Lock()
                res[d.Input] = fq
                lock.Unlock()
            } else {
                if *zip {
                    err = fq.WriteGzip(fmt.Sprintf("%v.fastq.gz", d.Prefix))
                } else {
                    err = fq.Write(fmt.Sprintf("%v.fastq", d.Prefix))
                }
                if err != nil {
                    log.Fatalf("Error -:- Write : %v\n", err)
                }
            }

        }(inp, headers, *substr, *exact, *exclude, *warn, d.Stdout)
    }
    wg.Wait()

    if d.Stdout {
        for _, desc := range d.Descr {
            err = res[desc.Input].Write("-")
            if err != nil {
                log.Fatalf("Error -:- Write : %v\n", err)
            }
        }
    }
}
